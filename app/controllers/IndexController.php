<?php

use Phalcon\Mvc\View;

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->setRenderLevel(
            View::LEVEL_LAYOUT
        );
        $actors=new Actors();
        $list=$actors->find(['limit' => 20]);
        $this->view->render('index', 'index');
    }
    
    public function getSearchListAction()
    {
        $this->view->disable();
	$data = $this->request->getJsonRawBody();        
        if (!empty($data->search)){
            $actors=new Actors();
            $list=$actors->query()
                ->orWhere('first_name like :fn: OR aliases like :al:',['fn'=>'%'.$data->search.'%','al'=>'%'.$data->search.'%'])
                ->limit(20)
                ->execute();            
        } else {
            $list="";
        }
        echo json_encode($list);
    }
    
}

