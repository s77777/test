<!DOCTYPE html>
<html lang=ru>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        {% block title %} {{ get_title() }} {% endblock %}
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/style.css"/>    
    </head>
    <body style="margin-top: 2em;">
        <div class="container">
                <header>
                    <div class="col-lg-12">
                        <span class="title">ПРО ЗВЕЗД</span>
                    </div>
                </header>
                 <div class="col-lg-12">
                    <br/>
                </div>
                <div id="result" class="col-lg-12">
                        <ul class="media-list">
                         <li class="media">
                           <a class="pull-left" href="#">
                             <img class="media-object" src="/img/stars.jpeg" alt="...">
                           </a>
                           <div class="media-body">
                             <h4 class="media-heading"><?php echo $result->first_name;?></h4>
                                <div class="media">
                                 <div class="media-body">
                                    <h4 class="media-heading">Псевдоним:</h4>
                                    <?php echo $result->aliases;?>
                                 </div>
                                </div>
                                <div class="media">
                                 <div class="media-body">
                                    <h4 class="media-heading">Дата рождения:</h4>
                                    <?php echo date("d-m-Y",strtotime($result->date_birth));?>
                                 </div>
                                </div>
                                <div class="media">
                                 <div class="media-body">
                                    <h4 class="media-heading">Место рождения:</h4>
                                    <?php echo $result->place_birth;?>
                                 </div>
                                </div>
                                <div class="media">
                                 <div class="media-body">
                                    <h4 class="media-heading">Страна проживания:</h4>
                                    <?php echo $result->country_residence;?>
                                 </div>
                                </div>
                                <div class="media">
                                 <div class="media-body">
                                    <h4 class="media-heading">Карьерный статус:</h4>
                                    <?php echo $result->career_status;?>
                                 </div>
                                </div>
                                <div class="media">
                                 <div class="media-body">
                                    <h4 class="media-heading">Рост:</h4>
                                    <?php echo $result->growth;?>
                                 </div>
                                </div>
                                <div class="media">
                                 <div class="media-body">
                                    <h4 class="media-heading">Цвет глаз:</h4>
                                    <?php echo $result->eye_color;?>
                                 </div>
                                </div>
                                <div class="media">
                                 <div class="media-body">
                                    <h4 class="media-heading">Цвет волос:</h4>
                                    <?php echo $result->hair_color;?>
                                 </div>
                                </div>
                                <div class="media">
                                 <div class="media-body">
                                    <h4 class="media-heading">Страница в соц.сетях:</h4>
                                    <?php echo $result->social_pages;?>
                                 </div>
                                </div>                                
                           </div>
                         </li>
                       </ul>
                </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="/js/vanilla.js"></script>
        <script src="/js/utils.js"></script>
        <script type="text/javascript">

        </script>
    </body>
</html>