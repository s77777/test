<?php

class Actors extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=10, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="first_name", type="string", length=100, nullable=true)
     */
    public $first_name;

    /**
     *
     * @var string
     * @Column(column="aliases", type="string", length=100, nullable=true)
     */
    public $aliases;

    /**
     *
     * @var string
     * @Column(column="date_birth", type="string", nullable=true)
     */
    public $date_birth;

    /**
     *
     * @var string
     * @Column(column="place_birth", type="string", length=250, nullable=true)
     */
    public $place_birth;

    /**
     *
     * @var string
     * @Column(column="country_residence", type="string", length=10, nullable=true)
     */
    public $country_residence;

    /**
     *
     * @var string
     * @Column(column="career_status", type="string", length=100, nullable=true)
     */
    public $career_status;

    /**
     *
     * @var string
     * @Column(column="eye_color", type="string", length=20, nullable=true)
     */
    public $eye_color;

    /**
     *
     * @var string
     * @Column(column="hair_color", type="string", length=20, nullable=true)
     */
    public $hair_color;

    /**
     *
     * @var integer
     * @Column(column="growth", type="integer", length=4, nullable=true)
     */
    public $growth;

    /**
     *
     * @var string
     * @Column(column="social_pages", type="string", length=250, nullable=true)
     */
    public $social_pages;

    /**
     *
     * @var string
     * @Column(column="images", type="string", length=100, nullable=true)
     */
    public $images;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("test");
        $this->setSource("actors");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'actors';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Actors[]|Actors|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Actors|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
