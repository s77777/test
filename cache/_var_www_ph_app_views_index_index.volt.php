<!DOCTYPE html>
<html lang=ru>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Task test</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css" />    
    </head>
    <body style="margin-top: 2em;">
        <div class="container">
                <header>
                    <div class="col-lg-6">
                        <span class="title">ПРО ЗВЕЗД</span>
                    </div>
                </header>
                <div class="col-lg-12">
                    <div class="input-group">
                         <input id='search' type="text" class="form-control" placeholder="Поиск ...">
                         <span class="input-group-btn">
                           <button id="search_button" class="btn btn-info" type="button">ОК</button>
                         </span>
                    </div>
                </div>
                <div class="col-lg-12">
                    <br/>
                </div>
                <div id="result" class="col-lg-12">
                    <div class="list-group">
                        <?php foreach($list as $key=>$value) {?>
                        <a id="a<?php echo $key;?>" href="<?php echo 'Actor/Index/'.$value->id;?>" class="list-group-item list-group-item-action flex-column align-items-start">
                            <div class="d-flex w-100 justify-content-between">
                                <h4 class="mb-1"><?php echo $value->first_name;?></h4>
                            </div>
                            <p class="mb-1"><?php echo 'Псевдоним: '.$value->aliases.'; Дата рождения: '.$value->date_birth.'; Место рождения: '.$value->place_birth.
                            ' Страна проживания: '.$value->country_residence.' Карьерный статус: '.$value->career_status;?></p>
                                <small><?php echo 'Рост: '.$value->growth.' Цвет глаз: '.$value->eye_color.' Цвет волос: '.$value->hair_color;?></small>
                                <span class="badge badge-default badge-pill" rel="a<?php echo $key;?>">X</span>
                        </a>
                        <?php }?>
                    </div>
                </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="js/vanilla.js"></script>
        <script src="js/utils.js"></script>
        <script type="text/javascript">
            window.addEventListener('DOMContentLoaded', function() {
                var search=d.getElementById('search');
                search.addEventListener('keyup',function(e){
                    e.preventDefault();
                    if (e.target.value.length>3) {
                        var obj={'search':e.target.value};
                        JsonRequest('Index/getSearchList',obj,getListSearchActor);
                    } else if (e.keyCode==13 && e.target.value!='') {
                        var obj={'search':e.target.value};
                        JsonRequest('Index/getSearchList',obj,getListResult);
                    }
                    });
                var del=d.getElementById('result').querySelectorAll('.list-group > a > span');
                for (var i=0;i<del.length;i++) {
                    del[i].addEventListener('click',deleting);
                }
                var agroup=d.getElementById('result').querySelectorAll('.list-group > a');
                for (var i=0;i<agroup.length;i++) {
                    agroup[i].addEventListener('mouseover',setActive);
                    agroup[i].addEventListener('mouseout',removeActive);
                }
                d.addEventListener('keyup',showActor,false);
                d.getElementById('search_button').addEventListener('click',is_selected);
            });
        </script>
    </body>
</html>