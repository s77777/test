<?php

use Phalcon\Mvc\View;

class ActorController extends ControllerBase
{
    protected  $Id;
    
    public function indexAction()
    {
        $this->tag->setTitle('Test');
        $this->view->setRenderLevel(
            View::LEVEL_LAYOUT
        );
	$params=$this->dispatcher->getParams('params');
	$this->Id=$params[0];
        $actors=new Actors();
        $result=$actors->findFirst($this->Id);
        $this->view->render('actor', 'index');    
    }
    
    

}
