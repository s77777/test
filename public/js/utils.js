var d=document;

function JsonRequest(url,objdata,callback) {   
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onload = callback;
        xhr.send(JSON.stringify(objdata));  
}

function getListSearchActor(e) {
    e.preventDefault();
    if (e.target.status === 200) {
        var result=JSON.parse(e.target.responseText);      
        if (result.length>0) {
            var select=d.getElementById('load_search');
            if (null==select) {
                var attrs = {'id':'load_search','size': 6,'name':'load_search','class':'search'};
                select=new Element('select',attrs);
                select.addEventListener('keyup',is_selected);
            }
            for(var i=0;i<result.length;i++){
                var option=new Element('option');
                option.textContent=result[i].first_name;
                option.addEventListener('click',is_selected);
                select.appendChild(option);
            }
            var search=d.getElementById('search');
            var cor=search.getBoundingClientRect();
            select.style.top=cor.top+cor.height+'px' ;
            select.style.left=cor.left+'px';
            select.style.width=cor.width+'px';
            d.body.appendChild(select);
            select.focus();
        }
    }
    else if (e.target.status !== 200) {
        console.log('Request error.  Returned status of ' + e.target.status);
    }
}

function getListResult(e) {
    e.preventDefault();
    if (e.target.status === 200) {
        removeLoadSearch();
        var result=JSON.parse(e.target.responseText);
        var elemParent=d.getElementById('result').querySelector('.list-group');
        elemParent.innerHTML='';
        if (result.length>0) {
            renderResult(result,elemParent);    
        } else {
            var p=new Element('p',{'class':'mb-1'});
            p.textContent='По Вашему запросу ничего не найдено, попробуйте ввести другой запрос!';
            elemParent.appendChild(p);
        }
    }
    else if (e.target.status !== 200) {
        console.log('Request error.  Returned status of ' + e.target.status);
    }   
}

function is_selected(e) {
    var search=d.getElementById('search');
    if (e.target.id!='search_button') {
        search.value=e.target.value;
    }
    if (search.value=='') {
        removeLoadSearch();
        return false;
    }
    if (e.key!=undefined && e.keyCode!=13) {
        removeLoadSearch();
        return false;
    }
    var obj={'search':search.value};
    JsonRequest('Index/getSearchList',obj,getListResult);
}

function Element(tag,attr) {
    var elem=document.createElement(tag);
    if (attr) {
	for (var key in attr) {
            elem.setAttribute(key,
             (key!='style')?attr[key]:setStyle(attr[key])
            ); 
	}
    }
    function setStyle(args) {
	var style=[];
	for (var css in args) {
	    style.push(css+':'+args[css])
	}
	return style.join(';');
    }
    return elem;
}

function renderResult(result,elemParent) {
    for(var i=0;i<result.length;i++){
        var a=new Element('a',{'id':'a'+i,'href':'Actor/Index/'+result[i].id,
                          'class':'list-group-item list-group-item-action flex-column align-items-start',});
        var div=new Element('div',{'class':'d-flex w-100 justify-content-between'});
        a.addEventListener('mouseover',showActor);
        var h4=new Element('h4',{'class':'mb-1'});
        h4.textContent=result[i].first_name;
        div.appendChild(h4);
        a.appendChild(div);
        var p=new Element('p',{'class':'mb-1'});
        p.textContent='Псевдоним: '+result[i].aliases+'; Дата рождения: '+result[i].date_birth+'; Место рождения: '+result[i].place_birth+
                     ' Страна проживания: '+result[i].country_residence+' Карьерный статус: '+result[i].career_status;
        a.appendChild(p);
        var small=new Element('small');
        small.textContent='Рост: '+result[i].growth+' Цвет глаз: '+result[i].eye_color+' Цвет волос: '+result[i].hair_color;
        a.appendChild(small);
        var span=new Element('span',{'class':'badge badge-default badge-pill','rel':'a'+i});
        span.textContent='X';
        span.addEventListener('click',deleting);
        a.appendChild(span);
        elemParent.appendChild(a);
    }
}

function deleting(e) {
    e.preventDefault();
    var a=e.target.getAttribute('rel')
    d.getElementById(a).remove();
}

function removeLoadSearch() {
    var ls=d.getElementById('load_search');
    if (null!=ls) ls.remove();
    return true;
}

function showActor(e) {
    var active=d.querySelector('a.active');
    if (null!=active) {
        window.location.assign(active.href);
    }
    
}

function setActive(e) {
    if (e.target.tagName=='A') {
        e.target.className+=' active';
    }
}

function removeActive(e) {
    if (e.target.tagName=='A') {
        var cls=e.target.className;
        e.target.className=cls.replace(' active','');
    }
}